all: shac

shac: main.c sha1.c sha1.h
	gcc -O3 -Wall -Wextra -std=c99 main.c sha1.c -oshac

clean:
	rm -f shac
