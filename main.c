#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sha1.h"

// fill buf with a random string from the builtin alphabet; stop at %len-1
// (i.e. the buffer must be at least %len chars big)
void random_str(char *buf, int len) {
    static const char alphabet[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for(int i=0; i<len; ++i) {
        buf[i] = alphabet[rand() % (sizeof(alphabet)-1)];
    }
    buf[len] = 0;
}

// hex digit => corresponding value; -1 if invalid
int hexval(char ch) {
    if('0'<=ch && ch<='9') return ch-'0';
    if('A'<=ch && ch<='F') return ch-'A'+10;
    if('a'<=ch && ch<='f') return ch-'a'+10;
    return -1;
}

// Parse a hex string from %src and write the bytes result into %target (maximum %size bytes);
// returns the point of %src where the parsing stopped
const char *hex2bytes(const char *src, uint8_t *target, size_t size) {
    uint8_t *end = target + size;
    for(; *src && target != end; src += 2, ++target) {
        int high = hexval(src[0]);
        if(high<0) break;
        int low = hexval(src[1]);
        if(low<0) break;
        *target = (high<<4) | low;
    }
    return src;
}

int main(int argc, char *argv[]) {
    SHA1_CTX ctx;
    char str[11];
    uint8_t digest[SHA1_DIGEST_SIZE];
    unsigned counter = 0;
    // Target hash; SHA1 for "heya"
    uint8_t target[] = "\x9b\x55\xce\x38\xe1\x07\x49\x28\x83\x0b\x37\xbd\xa5\xdd\xf3\x25\xac\x70\xa3\xd7";
    if(argc >= 2) {
        // if there's an argument, use it as target hash
        char *hex_hash = argv[1];
        const char *end = hex2bytes(hex_hash, target, sizeof(target));
        // if we stopped early or due to garbage the hash was not valid
        if(*end!=0 || (end-hex_hash) != sizeof(digest)*2) {
            printf("Invalid hash value\n");
            return 1;
        }
    }
    for(;;) {
        int size = rand()%10+1;
        random_str(str, size);
        SHA1_Init(&ctx);
        SHA1_Update(&ctx, (uint8_t *)str, size);
        SHA1_Final(&ctx, digest);
        if(memcmp(target, digest, sizeof(digest))==0) {
            printf("Found it! The original string value was %s (found after %u iterations)\n", str, counter);
            break;
        }
        ++counter;
        if(counter % 1000000 == 0) {
            printf("%u\n", counter);
        }
    }
    return 0;
}
